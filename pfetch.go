package main

import (
	"./src"
	"fmt"
	"os"
	"os/user"
	"runtime"
)

/** data types **/
type Fetch struct {
	Username        string
	Hostname        string
	OperatingSystem string
	Model           string
	Kernel          string
	Uptime          string
	Packages        string
	Shell           string
	CPU             string
	Memory			string
}

const (
	LINUX = iota
	BSD
	PLAN9
	DARWIN
	WINDOWS
	SOLARIS
)

/* global vars */
var os_const int

func get_goos() int {
	os := runtime.GOOS
	switch os {
	case "linux":
		return LINUX
	default:
		return LINUX
	}
}

func get_os(os chan string) {
	switch os_const {
	case LINUX:
		os <-linux.GetOsName()
	}
}

func get_username(c chan string) {
	current, _ := user.Current()
	c <- current.Username
}

func get_hostname(c chan string) {
	hostname, _ := os.Hostname()
	c <- hostname
}

func get_model(c chan string) {
	var model string

	switch os_const {
	case LINUX:
		model = linux.GetModel()
	}

	c <- model
}

func get_kernel(c chan string) {
	var kernel string

	switch os_const {
	case LINUX:
		kernel = linux.GetKernel()
	}

	c <- kernel
}

func get_uptime(c chan string) {
	var uptime string

	switch os_const {
	case LINUX:
		uptime = linux.GetUptime()
	}

	c <- uptime
}

/* todo: implement this */
func get_packages(c chan string) {
	c <- linux.GetPackages()
}

func get_shell(c chan string) {
	c <- linux.GetShell()
}

func get_cpu(c chan string) {
	var cpu string

	switch os_const {
	case LINUX:
		cpu = linux.GetCpu()
	}

	c <- cpu
}

func get_memory(c chan string) {
	var mem string

	switch os_const {
	case LINUX:
		mem = linux.GetMemory()
	}

	c <-mem
}


func main() {
	var fetch Fetch

	/* get os, for faster functions later on (no testing for every os) */
	os_const = get_goos()

	/* Channels */
	os := make(chan string)
	username := make(chan string)
	hostname := make(chan string)
	model := make(chan string)
	kernel := make(chan string)
	uptime := make(chan string)
	packages := make(chan string)
	shell := make(chan string)
	cpu := make(chan string)
	memory := make(chan string)

	/* functions for said channels */
	go get_os(os)
	go get_username(username)
	go get_hostname(hostname)
	go get_model(model)
	go get_kernel(kernel)
	go get_uptime(uptime)
	go get_packages(packages)
	go get_shell(shell)
	go get_cpu(cpu)
	go get_memory(memory)

	/* load each channel into variable */
	fetch.Username = <-username
	fetch.Hostname = <-hostname
	fetch.OperatingSystem = <-os
	fetch.Model = <-model
	fetch.Kernel = <-kernel
	fetch.Uptime = <-uptime
	fetch.Packages = <-packages
	fetch.Shell = <-shell
	fetch.CPU = <-cpu
	fetch.Memory = <-memory

	/* print shit */
	fmt.Println(fetch.Username, "@", fetch.Hostname)
	fmt.Println("OS:", fetch.OperatingSystem)
	fmt.Println("----------")
	fmt.Println("Model:", fetch.Model)
	fmt.Println("Kernel:", fetch.Kernel)
	fmt.Println("Uptime:", fetch.Uptime)
	fmt.Println("Shell:", fetch.Shell)
	fmt.Println("Packages:", fetch.Packages)
	fmt.Println("CPU:", fetch.CPU)
	fmt.Println("Memory:", fetch.Memory)
}
