package linux

import (
	"bufio"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"unicode"
)

func handle(e error) {
	if e != nil {
		panic(e)
	}
}

/* remove trailing chars */
func format(s []byte) string {
	return strings.TrimSpace(string(s))
}

func GetOsName() string {
	issue, err := ioutil.ReadFile("/etc/issue.net")
	handle(err)
	return string(issue)
}

func GetModel() string {
	file, err := ioutil.ReadFile("/sys/devices/virtual/dmi/id/product_version")
	handle(err)
	return format(file)
}

func GetKernel() string {
	data, err := ioutil.ReadFile("/proc/sys/kernel/osrelease")
	handle(err)
	return format(data)
}

func GetUptime() string {
	cmd, err := exec.Command("uptime", "-p").Output()
	handle(err)
	return format(cmd)
}

func GetPackages() string {
	cmd := "dpkg -l | wc -l"
	out, err := exec.Command("sh", "-c", cmd).Output()
	handle(err)
	return format(out)
}

func GetShell() string {
	return "Bash, probably.."
}

func GetCpu() string {
	file, _ := os.Open("/proc/cpuinfo")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var cpu string
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "model name") {
			cpu = scanner.Text()
			cpu = cpu[13:]
		}
	}

	return strings.TrimSpace(cpu)
}

func GetMemory() string {
	file, err := os.Open("/proc/meminfo")
	handle(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var used string
	var max string
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "MemTotal") {
			max = scanner.Text()
		} else if strings.Contains(scanner.Text(), "Active") {
			used = scanner.Text()
		}
	}

	for i, c := range max {
		if unicode.IsNumber(c) {
			max = max[i:]
			break
		}
	}

	for i, c := range used {
		if unicode.IsNumber(c) {
			used = used[i:]
			break
		}
	}
	return used + "/" + max
}
